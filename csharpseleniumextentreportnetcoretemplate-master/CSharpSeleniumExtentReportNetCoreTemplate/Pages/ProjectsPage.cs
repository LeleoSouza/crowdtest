﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class ProjectsPage : PageBase
    {
        #region Mapping

        By casoDeTestButton = By.Id("mat-tab-label-0-2");
        By casoDeTestText = By.XPath("//*[contains (text(),'Cadastrar Release com Sucesso')]");
        #endregion

        #region Actions

        public void ClicarEmCasosDeTeste()
        {
            Click(casoDeTestButton);
        }
        public string pegarTextoDeUmCasoDeTeste()
        {
            return GetText(casoDeTestText);
            
        }
        #endregion
    }
}