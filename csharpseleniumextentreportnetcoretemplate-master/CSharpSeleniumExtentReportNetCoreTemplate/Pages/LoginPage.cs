using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class LoginPage : PageBase
    {
        #region Mapping
        By usernameField = By.Id("login");
        By passwordField = By.Id("password");
        By loginButton = By.XPath("//*[contains(text(),'ENTRAR')]");
        By prosseguirButton = By.XPath("//*[contains(text(),'Prosseguir')]");
        #endregion

        #region Actions
        public void PreencherUsuario(string usuario)
        {
            SendKeys(usernameField, usuario);
        }

        public void PreencherSenha(string senha)
        {
            SendKeys(passwordField, senha);
        }

        public void ClicarEmLogin()
        {
            Click(loginButton);
        }

        public void ClicarEmProsseguir()
        {
            Click(prosseguirButton);
        }
        
        
        #endregion
    }

    
}
          

    


