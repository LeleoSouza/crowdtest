﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class ManagerDashboardPage : PageBase
    {
        
        #region Mapping

        By projetosButton = By.XPath("//li[@class ='li-projects']//a[@href= '/projects']");
        By treinamentoButton = By.XPath("//*[contains (text(),'Treinamento - Leonardo Moreira')]");
        
        #endregion

        #region Actions

        public void ClicarEmProjetos()
        {
            Click(projetosButton);
        }
        public void ClicarEmTreinamentoLeonardoMoreira()
        {
            Click(treinamentoButton);
        }
        #endregion
    }
}
