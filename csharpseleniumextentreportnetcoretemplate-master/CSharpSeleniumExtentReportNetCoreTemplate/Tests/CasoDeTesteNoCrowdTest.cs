﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class CasoDeTesteNoCrowdTest : TestBase
    {

        #region Pages and Flows Objects
        LoginPage loginPage;
        LoginFlows loginFlows;
        ManagerDashboardPage managerDashboardPage;
        ProjectsPage projectsPage;
        
        #endregion

        [Test]
        public void VerificarPresencaDeCasoDeTesteNoCrowdTest()
        {
            loginPage = new LoginPage();
            managerDashboardPage = new ManagerDashboardPage();
            projectsPage = new ProjectsPage();
            loginFlows = new LoginFlows();
                        

            #region Parameters
            string usuario = "leonardo.souza@base2.com.br";
            string senha = "D0wn1993";
            string textoCasoDeTeste = "Cadastrar Release com Sucesso";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);

            managerDashboardPage.ClicarEmProjetos();
            managerDashboardPage.ClicarEmTreinamentoLeonardoMoreira();
            
            projectsPage.ClicarEmCasosDeTeste();
            projectsPage.pegarTextoDeUmCasoDeTeste();
            
            Assert.AreEqual(textoCasoDeTeste, projectsPage.pegarTextoDeUmCasoDeTeste());
           




        }

    }
}
